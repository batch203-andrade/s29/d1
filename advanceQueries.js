// Advance Query Operators
// Flexible querying of data within MongoDB

//[Section] Comparison Query Operators

// $gt/$gte operator

db.users.find(
    {
        age: { $gt: 65 }
    }
)
db.users.find(
    {
        age: { $gte: 65 }
    }
)

// $lt/$lte operator

db.users.find(
    {
        age: { $lt: 65 }
    }
)

db.users.find(
    {
        age: { $lte: 65 }
    }
)

// $ne operator

db.users.find(
    {
        age: { $ne: 82 }
    }
)

// $in operator

db.users.find(
    {
        lastName: { $in: ['Stephen', 'Doe'] }
    }
)

db.users.find(
    {
        courses: { $in: ["React", "PHP", "CSS"] }
    }
)

//Logical Query Operators && || !


// $or operator

//Multiple field value pairs
db.users.find(
    {
        $or: [
            { firstName: "Neil" },
            { age: { $gt: 25 } }
        ]
    }
)

// $and operator 
db.users.find(
    {
        $and: [
            { age: { $ne: 82 } },
            { age: { $ne: 76 } },
            { age: { $ne: 21 } }
        ]
    }
)

db.users.find(
    {
        $and: [
            { age: { $ne: 82 } },
            { department: "none" }
        ]
    }
)

//Field Projection

//Inclusion

db.users.find(
    {
        firstName: "Jane"
    },
    {
        firstName: 1,
        lastName: 0,
        contact: 1,
    }
)

//Exclusion

db.users.find(
    {
        firstName: "Jane"
    },
    {
        firstName: 1,
        lastName: 1,
        contact: 1,
        _id: 0
    }
)

// Evaluation Query Operators
// $regex operator

db.users.find({
    firstName: { $regex: '', $options: '$i' }
})